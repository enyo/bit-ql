package com.xuanyue.db.xuan.core.index;

/**
 * 
 * @author 解观海
 * @email  guanhaixie@sina.cn
 * @date 2020年6月23日
 * @version 0.1
 */
public class LongIndex extends NumberIndex{
	@Override
	public byte getType() {
		return 2;
	}
}
