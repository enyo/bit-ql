// Generated from BitQ.g4 by ANTLR 4.4

	package com.xuanyue.db.xuan.antlr;

import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link BitQParser}.
 */
public interface BitQListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link BitQParser#orCondition}.
	 * @param ctx the parse tree
	 */
	void enterOrCondition(@NotNull BitQParser.OrConditionContext ctx);
	/**
	 * Exit a parse tree produced by {@link BitQParser#orCondition}.
	 * @param ctx the parse tree
	 */
	void exitOrCondition(@NotNull BitQParser.OrConditionContext ctx);
	/**
	 * Enter a parse tree produced by {@link BitQParser#orNot}.
	 * @param ctx the parse tree
	 */
	void enterOrNot(@NotNull BitQParser.OrNotContext ctx);
	/**
	 * Exit a parse tree produced by {@link BitQParser#orNot}.
	 * @param ctx the parse tree
	 */
	void exitOrNot(@NotNull BitQParser.OrNotContext ctx);
	/**
	 * Enter a parse tree produced by {@link BitQParser#tjoinPart}.
	 * @param ctx the parse tree
	 */
	void enterTjoinPart(@NotNull BitQParser.TjoinPartContext ctx);
	/**
	 * Exit a parse tree produced by {@link BitQParser#tjoinPart}.
	 * @param ctx the parse tree
	 */
	void exitTjoinPart(@NotNull BitQParser.TjoinPartContext ctx);
	/**
	 * Enter a parse tree produced by {@link BitQParser#groupCondition}.
	 * @param ctx the parse tree
	 */
	void enterGroupCondition(@NotNull BitQParser.GroupConditionContext ctx);
	/**
	 * Exit a parse tree produced by {@link BitQParser#groupCondition}.
	 * @param ctx the parse tree
	 */
	void exitGroupCondition(@NotNull BitQParser.GroupConditionContext ctx);
	/**
	 * Enter a parse tree produced by {@link BitQParser#andCondition}.
	 * @param ctx the parse tree
	 */
	void enterAndCondition(@NotNull BitQParser.AndConditionContext ctx);
	/**
	 * Exit a parse tree produced by {@link BitQParser#andCondition}.
	 * @param ctx the parse tree
	 */
	void exitAndCondition(@NotNull BitQParser.AndConditionContext ctx);
	/**
	 * Enter a parse tree produced by {@link BitQParser#phone_seach}.
	 * @param ctx the parse tree
	 */
	void enterPhone_seach(@NotNull BitQParser.Phone_seachContext ctx);
	/**
	 * Exit a parse tree produced by {@link BitQParser#phone_seach}.
	 * @param ctx the parse tree
	 */
	void exitPhone_seach(@NotNull BitQParser.Phone_seachContext ctx);
	/**
	 * Enter a parse tree produced by {@link BitQParser#query}.
	 * @param ctx the parse tree
	 */
	void enterQuery(@NotNull BitQParser.QueryContext ctx);
	/**
	 * Exit a parse tree produced by {@link BitQParser#query}.
	 * @param ctx the parse tree
	 */
	void exitQuery(@NotNull BitQParser.QueryContext ctx);
	/**
	 * Enter a parse tree produced by {@link BitQParser#repo}.
	 * @param ctx the parse tree
	 */
	void enterRepo(@NotNull BitQParser.RepoContext ctx);
	/**
	 * Exit a parse tree produced by {@link BitQParser#repo}.
	 * @param ctx the parse tree
	 */
	void exitRepo(@NotNull BitQParser.RepoContext ctx);
	/**
	 * Enter a parse tree produced by {@link BitQParser#saveAsFile}.
	 * @param ctx the parse tree
	 */
	void enterSaveAsFile(@NotNull BitQParser.SaveAsFileContext ctx);
	/**
	 * Exit a parse tree produced by {@link BitQParser#saveAsFile}.
	 * @param ctx the parse tree
	 */
	void exitSaveAsFile(@NotNull BitQParser.SaveAsFileContext ctx);
	/**
	 * Enter a parse tree produced by {@link BitQParser#values}.
	 * @param ctx the parse tree
	 */
	void enterValues(@NotNull BitQParser.ValuesContext ctx);
	/**
	 * Exit a parse tree produced by {@link BitQParser#values}.
	 * @param ctx the parse tree
	 */
	void exitValues(@NotNull BitQParser.ValuesContext ctx);
	/**
	 * Enter a parse tree produced by {@link BitQParser#fullName}.
	 * @param ctx the parse tree
	 */
	void enterFullName(@NotNull BitQParser.FullNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link BitQParser#fullName}.
	 * @param ctx the parse tree
	 */
	void exitFullName(@NotNull BitQParser.FullNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link BitQParser#conditionElement}.
	 * @param ctx the parse tree
	 */
	void enterConditionElement(@NotNull BitQParser.ConditionElementContext ctx);
	/**
	 * Exit a parse tree produced by {@link BitQParser#conditionElement}.
	 * @param ctx the parse tree
	 */
	void exitConditionElement(@NotNull BitQParser.ConditionElementContext ctx);
	/**
	 * Enter a parse tree produced by {@link BitQParser#sortE}.
	 * @param ctx the parse tree
	 */
	void enterSortE(@NotNull BitQParser.SortEContext ctx);
	/**
	 * Exit a parse tree produced by {@link BitQParser#sortE}.
	 * @param ctx the parse tree
	 */
	void exitSortE(@NotNull BitQParser.SortEContext ctx);
	/**
	 * Enter a parse tree produced by {@link BitQParser#result}.
	 * @param ctx the parse tree
	 */
	void enterResult(@NotNull BitQParser.ResultContext ctx);
	/**
	 * Exit a parse tree produced by {@link BitQParser#result}.
	 * @param ctx the parse tree
	 */
	void exitResult(@NotNull BitQParser.ResultContext ctx);
	/**
	 * Enter a parse tree produced by {@link BitQParser#to_date}.
	 * @param ctx the parse tree
	 */
	void enterTo_date(@NotNull BitQParser.To_dateContext ctx);
	/**
	 * Exit a parse tree produced by {@link BitQParser#to_date}.
	 * @param ctx the parse tree
	 */
	void exitTo_date(@NotNull BitQParser.To_dateContext ctx);
	/**
	 * Enter a parse tree produced by {@link BitQParser#tablePart}.
	 * @param ctx the parse tree
	 */
	void enterTablePart(@NotNull BitQParser.TablePartContext ctx);
	/**
	 * Exit a parse tree produced by {@link BitQParser#tablePart}.
	 * @param ctx the parse tree
	 */
	void exitTablePart(@NotNull BitQParser.TablePartContext ctx);
	/**
	 * Enter a parse tree produced by {@link BitQParser#boolTF}.
	 * @param ctx the parse tree
	 */
	void enterBoolTF(@NotNull BitQParser.BoolTFContext ctx);
	/**
	 * Exit a parse tree produced by {@link BitQParser#boolTF}.
	 * @param ctx the parse tree
	 */
	void exitBoolTF(@NotNull BitQParser.BoolTFContext ctx);
	/**
	 * Enter a parse tree produced by {@link BitQParser#conditionExpr}.
	 * @param ctx the parse tree
	 */
	void enterConditionExpr(@NotNull BitQParser.ConditionExprContext ctx);
	/**
	 * Exit a parse tree produced by {@link BitQParser#conditionExpr}.
	 * @param ctx the parse tree
	 */
	void exitConditionExpr(@NotNull BitQParser.ConditionExprContext ctx);
	/**
	 * Enter a parse tree produced by {@link BitQParser#limit}.
	 * @param ctx the parse tree
	 */
	void enterLimit(@NotNull BitQParser.LimitContext ctx);
	/**
	 * Exit a parse tree produced by {@link BitQParser#limit}.
	 * @param ctx the parse tree
	 */
	void exitLimit(@NotNull BitQParser.LimitContext ctx);
	/**
	 * Enter a parse tree produced by {@link BitQParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterExpr(@NotNull BitQParser.ExprContext ctx);
	/**
	 * Exit a parse tree produced by {@link BitQParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitExpr(@NotNull BitQParser.ExprContext ctx);
	/**
	 * Enter a parse tree produced by {@link BitQParser#sortBy}.
	 * @param ctx the parse tree
	 */
	void enterSortBy(@NotNull BitQParser.SortByContext ctx);
	/**
	 * Exit a parse tree produced by {@link BitQParser#sortBy}.
	 * @param ctx the parse tree
	 */
	void exitSortBy(@NotNull BitQParser.SortByContext ctx);
	/**
	 * Enter a parse tree produced by {@link BitQParser#mix}.
	 * @param ctx the parse tree
	 */
	void enterMix(@NotNull BitQParser.MixContext ctx);
	/**
	 * Exit a parse tree produced by {@link BitQParser#mix}.
	 * @param ctx the parse tree
	 */
	void exitMix(@NotNull BitQParser.MixContext ctx);
	/**
	 * Enter a parse tree produced by {@link BitQParser#andNot}.
	 * @param ctx the parse tree
	 */
	void enterAndNot(@NotNull BitQParser.AndNotContext ctx);
	/**
	 * Exit a parse tree produced by {@link BitQParser#andNot}.
	 * @param ctx the parse tree
	 */
	void exitAndNot(@NotNull BitQParser.AndNotContext ctx);
}