// Generated from BitQ.g4 by ANTLR 4.4

	package com.xuanyue.db.xuan.antlr;

import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class BitQLexer extends Lexer {
	static { RuntimeMetaData.checkVersion("4.4", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__7=1, T__6=2, T__5=3, T__4=4, T__3=5, T__2=6, T__1=7, T__0=8, TransArrt=9, 
		SAVE=10, AS=11, MIX=12, DESC=13, ASC=14, Phone_seach=15, PositionMatch=16, 
		Contains=17, Has_Every_Char=18, SELECT=19, FROM=20, WHERE=21, ON=22, LEFT=23, 
		RIGHT=24, JOIN=25, AND=26, OR=27, NOT=28, TO_DATE=29, ExprNot=30, COMMA=31, 
		SEMI=32, LIMIT=33, Order=34, By=35, NAME=36, DOT=37, Brackets_L=38, Brackets_R=39, 
		STRING=40, NUM=41, TRUE=42, FALSE=43, WS=44;
	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	public static final String[] tokenNames = {
		"'\\u0000'", "'\\u0001'", "'\\u0002'", "'\\u0003'", "'\\u0004'", "'\\u0005'", 
		"'\\u0006'", "'\\u0007'", "'\b'", "'\t'", "'\n'", "'\\u000B'", "'\f'", 
		"'\r'", "'\\u000E'", "'\\u000F'", "'\\u0010'", "'\\u0011'", "'\\u0012'", 
		"'\\u0013'", "'\\u0014'", "'\\u0015'", "'\\u0016'", "'\\u0017'", "'\\u0018'", 
		"'\\u0019'", "'\\u001A'", "'\\u001B'", "'\\u001C'", "'\\u001D'", "'\\u001E'", 
		"'\\u001F'", "' '", "'!'", "'\"'", "'#'", "'$'", "'%'", "'&'", "'''", 
		"'('", "')'", "'*'", "'+'", "','"
	};
	public static final String[] ruleNames = {
		"T__7", "T__6", "T__5", "T__4", "T__3", "T__2", "T__1", "T__0", "TransArrt", 
		"SAVE", "AS", "MIX", "DESC", "ASC", "Phone_seach", "PositionMatch", "Contains", 
		"Has_Every_Char", "SELECT", "FROM", "WHERE", "ON", "LEFT", "RIGHT", "JOIN", 
		"AND", "OR", "NOT", "TO_DATE", "ExprNot", "COMMA", "SEMI", "LIMIT", "Order", 
		"By", "NAME", "DOT", "Brackets_L", "Brackets_R", "STRING", "NUM", "TRUE", 
		"FALSE", "WS"
	};


	public BitQLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "BitQ.g4"; }

	@Override
	public String[] getTokenNames() { return tokenNames; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\2.\u014d\b\1\4\2\t"+
		"\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13"+
		"\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t \4!"+
		"\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t&\4\'\t\'\4(\t(\4)\t)\4*\t*\4+\t+\4"+
		",\t,\4-\t-\3\2\3\2\3\2\3\3\3\3\3\3\3\4\3\4\3\5\3\5\3\5\3\6\3\6\3\7\3\7"+
		"\3\b\3\b\3\t\3\t\3\n\3\n\6\nq\n\n\r\n\16\nr\3\13\3\13\3\13\3\13\3\13\3"+
		"\f\3\f\3\f\3\r\3\r\3\r\3\r\3\16\3\16\3\16\3\16\3\16\3\17\3\17\3\17\3\17"+
		"\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\21\3\21"+
		"\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\22\3\22"+
		"\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\23\3\23\3\23\3\23\3\23\3\23\3\23"+
		"\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\24\3\24\3\24\3\24\3\24\3\24"+
		"\3\24\3\25\3\25\3\25\3\25\3\25\3\26\3\26\3\26\3\26\3\26\3\26\3\27\3\27"+
		"\3\27\3\30\3\30\3\30\3\30\3\30\3\31\3\31\3\31\3\31\3\31\3\31\3\32\3\32"+
		"\3\32\3\32\3\32\3\33\3\33\3\33\3\33\3\34\3\34\3\34\3\35\3\35\3\35\3\35"+
		"\3\36\3\36\3\36\3\36\3\36\3\36\3\36\3\36\3\37\3\37\3 \3 \3!\3!\3\"\3\""+
		"\3\"\3\"\3\"\3\"\3#\3#\3#\3#\3#\3#\3$\3$\3$\3%\3%\7%\u010b\n%\f%\16%\u010e"+
		"\13%\3&\3&\3\'\3\'\3(\3(\3)\3)\3)\3)\7)\u011a\n)\f)\16)\u011d\13)\3)\3"+
		")\3)\3)\3)\7)\u0124\n)\f)\16)\u0127\13)\3)\5)\u012a\n)\3*\6*\u012d\n*"+
		"\r*\16*\u012e\3*\3*\6*\u0133\n*\r*\16*\u0134\5*\u0137\n*\3*\5*\u013a\n"+
		"*\3+\3+\3+\3+\3+\3,\3,\3,\3,\3,\3,\3-\6-\u0148\n-\r-\16-\u0149\3-\3-\2"+
		"\2.\3\3\5\4\7\5\t\6\13\7\r\b\17\t\21\n\23\13\25\f\27\r\31\16\33\17\35"+
		"\20\37\21!\22#\23%\24\'\25)\26+\27-\30/\31\61\32\63\33\65\34\67\359\36"+
		";\37= ?!A\"C#E$G%I&K\'M(O)Q*S+U,W-Y.\3\2\37\4\2UUuu\4\2CCcc\4\2XXxx\4"+
		"\2GGgg\4\2OOoo\4\2KKkk\4\2ZZzz\4\2FFff\4\2EEee\4\2RRrr\4\2JJjj\4\2QQq"+
		"q\4\2PPpp\4\2VVvv\4\2TTtt\4\2[[{{\4\2NNnn\4\2HHhh\4\2YYyy\4\2IIii\4\2"+
		"LLll\4\2DDdd\4\2C\\c|\6\2\62;C\\aac|\4\2))^^\4\2$$^^\4\2hhnn\4\2WWww\5"+
		"\2\13\f\17\17\"\"\u0158\2\3\3\2\2\2\2\5\3\2\2\2\2\7\3\2\2\2\2\t\3\2\2"+
		"\2\2\13\3\2\2\2\2\r\3\2\2\2\2\17\3\2\2\2\2\21\3\2\2\2\2\23\3\2\2\2\2\25"+
		"\3\2\2\2\2\27\3\2\2\2\2\31\3\2\2\2\2\33\3\2\2\2\2\35\3\2\2\2\2\37\3\2"+
		"\2\2\2!\3\2\2\2\2#\3\2\2\2\2%\3\2\2\2\2\'\3\2\2\2\2)\3\2\2\2\2+\3\2\2"+
		"\2\2-\3\2\2\2\2/\3\2\2\2\2\61\3\2\2\2\2\63\3\2\2\2\2\65\3\2\2\2\2\67\3"+
		"\2\2\2\29\3\2\2\2\2;\3\2\2\2\2=\3\2\2\2\2?\3\2\2\2\2A\3\2\2\2\2C\3\2\2"+
		"\2\2E\3\2\2\2\2G\3\2\2\2\2I\3\2\2\2\2K\3\2\2\2\2M\3\2\2\2\2O\3\2\2\2\2"+
		"Q\3\2\2\2\2S\3\2\2\2\2U\3\2\2\2\2W\3\2\2\2\2Y\3\2\2\2\3[\3\2\2\2\5^\3"+
		"\2\2\2\7a\3\2\2\2\tc\3\2\2\2\13f\3\2\2\2\rh\3\2\2\2\17j\3\2\2\2\21l\3"+
		"\2\2\2\23n\3\2\2\2\25t\3\2\2\2\27y\3\2\2\2\31|\3\2\2\2\33\u0080\3\2\2"+
		"\2\35\u0085\3\2\2\2\37\u0089\3\2\2\2!\u0095\3\2\2\2#\u00a3\3\2\2\2%\u00ac"+
		"\3\2\2\2\'\u00bb\3\2\2\2)\u00c2\3\2\2\2+\u00c7\3\2\2\2-\u00cd\3\2\2\2"+
		"/\u00d0\3\2\2\2\61\u00d5\3\2\2\2\63\u00db\3\2\2\2\65\u00e0\3\2\2\2\67"+
		"\u00e4\3\2\2\29\u00e7\3\2\2\2;\u00eb\3\2\2\2=\u00f3\3\2\2\2?\u00f5\3\2"+
		"\2\2A\u00f7\3\2\2\2C\u00f9\3\2\2\2E\u00ff\3\2\2\2G\u0105\3\2\2\2I\u0108"+
		"\3\2\2\2K\u010f\3\2\2\2M\u0111\3\2\2\2O\u0113\3\2\2\2Q\u0129\3\2\2\2S"+
		"\u012c\3\2\2\2U\u013b\3\2\2\2W\u0140\3\2\2\2Y\u0147\3\2\2\2[\\\7>\2\2"+
		"\\]\7?\2\2]\4\3\2\2\2^_\7#\2\2_`\7?\2\2`\6\3\2\2\2ab\7]\2\2b\b\3\2\2\2"+
		"cd\7@\2\2de\7?\2\2e\n\3\2\2\2fg\7>\2\2g\f\3\2\2\2hi\7_\2\2i\16\3\2\2\2"+
		"jk\7?\2\2k\20\3\2\2\2lm\7@\2\2m\22\3\2\2\2np\7A\2\2oq\4\62;\2po\3\2\2"+
		"\2qr\3\2\2\2rp\3\2\2\2rs\3\2\2\2s\24\3\2\2\2tu\t\2\2\2uv\t\3\2\2vw\t\4"+
		"\2\2wx\t\5\2\2x\26\3\2\2\2yz\t\3\2\2z{\t\2\2\2{\30\3\2\2\2|}\t\6\2\2}"+
		"~\t\7\2\2~\177\t\b\2\2\177\32\3\2\2\2\u0080\u0081\t\t\2\2\u0081\u0082"+
		"\t\5\2\2\u0082\u0083\t\2\2\2\u0083\u0084\t\n\2\2\u0084\34\3\2\2\2\u0085"+
		"\u0086\t\3\2\2\u0086\u0087\t\2\2\2\u0087\u0088\t\n\2\2\u0088\36\3\2\2"+
		"\2\u0089\u008a\t\13\2\2\u008a\u008b\t\f\2\2\u008b\u008c\t\r\2\2\u008c"+
		"\u008d\t\16\2\2\u008d\u008e\t\5\2\2\u008e\u008f\7a\2\2\u008f\u0090\t\2"+
		"\2\2\u0090\u0091\t\5\2\2\u0091\u0092\t\3\2\2\u0092\u0093\t\n\2\2\u0093"+
		"\u0094\t\f\2\2\u0094 \3\2\2\2\u0095\u0096\t\13\2\2\u0096\u0097\t\r\2\2"+
		"\u0097\u0098\t\2\2\2\u0098\u0099\t\7\2\2\u0099\u009a\t\17\2\2\u009a\u009b"+
		"\t\7\2\2\u009b\u009c\t\r\2\2\u009c\u009d\t\16\2\2\u009d\u009e\t\6\2\2"+
		"\u009e\u009f\t\3\2\2\u009f\u00a0\t\17\2\2\u00a0\u00a1\t\n\2\2\u00a1\u00a2"+
		"\t\f\2\2\u00a2\"\3\2\2\2\u00a3\u00a4\t\n\2\2\u00a4\u00a5\t\r\2\2\u00a5"+
		"\u00a6\t\16\2\2\u00a6\u00a7\t\17\2\2\u00a7\u00a8\t\3\2\2\u00a8\u00a9\t"+
		"\7\2\2\u00a9\u00aa\t\16\2\2\u00aa\u00ab\t\2\2\2\u00ab$\3\2\2\2\u00ac\u00ad"+
		"\t\f\2\2\u00ad\u00ae\t\3\2\2\u00ae\u00af\t\2\2\2\u00af\u00b0\7a\2\2\u00b0"+
		"\u00b1\t\5\2\2\u00b1\u00b2\t\4\2\2\u00b2\u00b3\t\5\2\2\u00b3\u00b4\t\20"+
		"\2\2\u00b4\u00b5\t\21\2\2\u00b5\u00b6\7a\2\2\u00b6\u00b7\t\n\2\2\u00b7"+
		"\u00b8\t\f\2\2\u00b8\u00b9\t\3\2\2\u00b9\u00ba\t\20\2\2\u00ba&\3\2\2\2"+
		"\u00bb\u00bc\t\2\2\2\u00bc\u00bd\t\5\2\2\u00bd\u00be\t\22\2\2\u00be\u00bf"+
		"\t\5\2\2\u00bf\u00c0\t\n\2\2\u00c0\u00c1\t\17\2\2\u00c1(\3\2\2\2\u00c2"+
		"\u00c3\t\23\2\2\u00c3\u00c4\t\20\2\2\u00c4\u00c5\t\r\2\2\u00c5\u00c6\t"+
		"\6\2\2\u00c6*\3\2\2\2\u00c7\u00c8\t\24\2\2\u00c8\u00c9\t\f\2\2\u00c9\u00ca"+
		"\t\5\2\2\u00ca\u00cb\t\20\2\2\u00cb\u00cc\t\5\2\2\u00cc,\3\2\2\2\u00cd"+
		"\u00ce\t\r\2\2\u00ce\u00cf\t\16\2\2\u00cf.\3\2\2\2\u00d0\u00d1\t\22\2"+
		"\2\u00d1\u00d2\t\5\2\2\u00d2\u00d3\t\23\2\2\u00d3\u00d4\t\17\2\2\u00d4"+
		"\60\3\2\2\2\u00d5\u00d6\t\20\2\2\u00d6\u00d7\t\7\2\2\u00d7\u00d8\t\25"+
		"\2\2\u00d8\u00d9\t\f\2\2\u00d9\u00da\t\17\2\2\u00da\62\3\2\2\2\u00db\u00dc"+
		"\t\26\2\2\u00dc\u00dd\t\r\2\2\u00dd\u00de\t\7\2\2\u00de\u00df\t\16\2\2"+
		"\u00df\64\3\2\2\2\u00e0\u00e1\t\3\2\2\u00e1\u00e2\t\16\2\2\u00e2\u00e3"+
		"\t\t\2\2\u00e3\66\3\2\2\2\u00e4\u00e5\t\r\2\2\u00e5\u00e6\t\20\2\2\u00e6"+
		"8\3\2\2\2\u00e7\u00e8\t\16\2\2\u00e8\u00e9\t\r\2\2\u00e9\u00ea\t\17\2"+
		"\2\u00ea:\3\2\2\2\u00eb\u00ec\t\17\2\2\u00ec\u00ed\t\r\2\2\u00ed\u00ee"+
		"\7a\2\2\u00ee\u00ef\t\t\2\2\u00ef\u00f0\t\3\2\2\u00f0\u00f1\t\17\2\2\u00f1"+
		"\u00f2\t\5\2\2\u00f2<\3\2\2\2\u00f3\u00f4\7#\2\2\u00f4>\3\2\2\2\u00f5"+
		"\u00f6\7.\2\2\u00f6@\3\2\2\2\u00f7\u00f8\7=\2\2\u00f8B\3\2\2\2\u00f9\u00fa"+
		"\t\22\2\2\u00fa\u00fb\t\7\2\2\u00fb\u00fc\t\6\2\2\u00fc\u00fd\t\7\2\2"+
		"\u00fd\u00fe\t\17\2\2\u00feD\3\2\2\2\u00ff\u0100\t\r\2\2\u0100\u0101\t"+
		"\20\2\2\u0101\u0102\t\t\2\2\u0102\u0103\t\5\2\2\u0103\u0104\t\20\2\2\u0104"+
		"F\3\2\2\2\u0105\u0106\t\27\2\2\u0106\u0107\t\21\2\2\u0107H\3\2\2\2\u0108"+
		"\u010c\t\30\2\2\u0109\u010b\t\31\2\2\u010a\u0109\3\2\2\2\u010b\u010e\3"+
		"\2\2\2\u010c\u010a\3\2\2\2\u010c\u010d\3\2\2\2\u010dJ\3\2\2\2\u010e\u010c"+
		"\3\2\2\2\u010f\u0110\7\60\2\2\u0110L\3\2\2\2\u0111\u0112\7*\2\2\u0112"+
		"N\3\2\2\2\u0113\u0114\7+\2\2\u0114P\3\2\2\2\u0115\u011b\7)\2\2\u0116\u011a"+
		"\n\32\2\2\u0117\u0118\7^\2\2\u0118\u011a\13\2\2\2\u0119\u0116\3\2\2\2"+
		"\u0119\u0117\3\2\2\2\u011a\u011d\3\2\2\2\u011b\u0119\3\2\2\2\u011b\u011c"+
		"\3\2\2\2\u011c\u011e\3\2\2\2\u011d\u011b\3\2\2\2\u011e\u012a\7)\2\2\u011f"+
		"\u0125\7$\2\2\u0120\u0124\n\33\2\2\u0121\u0122\7^\2\2\u0122\u0124\13\2"+
		"\2\2\u0123\u0120\3\2\2\2\u0123\u0121\3\2\2\2\u0124\u0127\3\2\2\2\u0125"+
		"\u0123\3\2\2\2\u0125\u0126\3\2\2\2\u0126\u0128\3\2\2\2\u0127\u0125\3\2"+
		"\2\2\u0128\u012a\7$\2\2\u0129\u0115\3\2\2\2\u0129\u011f\3\2\2\2\u012a"+
		"R\3\2\2\2\u012b\u012d\4\62;\2\u012c\u012b\3\2\2\2\u012d\u012e\3\2\2\2"+
		"\u012e\u012c\3\2\2\2\u012e\u012f\3\2\2\2\u012f\u0136\3\2\2\2\u0130\u0132"+
		"\5K&\2\u0131\u0133\4\62;\2\u0132\u0131\3\2\2\2\u0133\u0134\3\2\2\2\u0134"+
		"\u0132\3\2\2\2\u0134\u0135\3\2\2\2\u0135\u0137\3\2\2\2\u0136\u0130\3\2"+
		"\2\2\u0136\u0137\3\2\2\2\u0137\u0139\3\2\2\2\u0138\u013a\t\34\2\2\u0139"+
		"\u0138\3\2\2\2\u0139\u013a\3\2\2\2\u013aT\3\2\2\2\u013b\u013c\t\17\2\2"+
		"\u013c\u013d\t\20\2\2\u013d\u013e\t\35\2\2\u013e\u013f\t\5\2\2\u013fV"+
		"\3\2\2\2\u0140\u0141\t\23\2\2\u0141\u0142\t\3\2\2\u0142\u0143\t\22\2\2"+
		"\u0143\u0144\t\2\2\2\u0144\u0145\t\5\2\2\u0145X\3\2\2\2\u0146\u0148\t"+
		"\36\2\2\u0147\u0146\3\2\2\2\u0148\u0149\3\2\2\2\u0149\u0147\3\2\2\2\u0149"+
		"\u014a\3\2\2\2\u014a\u014b\3\2\2\2\u014b\u014c\b-\2\2\u014cZ\3\2\2\2\17"+
		"\2r\u010c\u0119\u011b\u0123\u0125\u0129\u012e\u0134\u0136\u0139\u0149"+
		"\3\b\2\2";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}